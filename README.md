[![N|Solid](https://www.cybermiles.io/twitterCardLogo.jpg)](https://www.cybermiles.io/)

# CyberMiles.io-v3 Documentation

### Tech Stack

* [GatsbyJS] - React based, GraphQL powered, static site generator. 
  --> fast page loads, service workers, code splitting, server-side rendering, intelligent image loading, asset optimization, data prefetching
* [ReactJS] - JavaScript library in Gatsby. 
* [GraphQL] - Data Layer in Gatsby that collects and queries data from various sources (prismic and coinmarketcap)
* [Netlify] - All in one workflow that handles our deployment and continuous integration
* [Prismic] - Headless CMS Backend for our blog posts
* [Zapier] - Sends scheduled daily webhook POST requests to build our site (fetches new data from the coinmarketcap api)
* [React-Intl] - API to format and handle all our translations
* [Styled Components] - Component-based styling architecture with scoping and conditional styles via props --> very react friendly
* [Grommet] - React-based UI component library that uses styled-components as well

[![N|Solid](https://www.cybermiles.io/Cybermiles.io.png)](https://www.cybermiles.io/Cybermiles.io.png)

### Installation
Install the dependencies and devDependencies and start the development server.
```sh
$ cd cybermiles-io-v3
$ npm install
$ gatsby develop
```
To create a production build
```sh
$ gatsby build
```
To serve the production build locally
```sh
$ gatsby serve
```
Deploy to production environment (www.cybermiles.io)
```sh
$ git checkout master
$ git push origin master
```
Deploy to staging environment (staging.cybermiles.io)
```sh
$ git checkout staging
$ git push origin staging
```

### Project Structure
```sh
/
|-- /.cache # Internal cache created automatically by Gatsby
|-- /config
    |-- website.js  # Variables used for SEO and other metadata across the site
|-- /public  # Output of `gatsby build` automatically generated into this folder
|-- /src  # Directory that contains everything you see on the frontend
    |-- /assets  # Images and other assets organized inside this folder
    |-- /components
        |-- /reusables  # React components that make up the pages (some are resuable)
        |-- layout.jsx  # Wrapper for every page besides blog post template (Injects styles, translations, SEO, header and footer)
        |-- seo.jsx  # Add title, meta attributes, etc into document Head
    |-- /fragments
        |-- fragments.js # Stores all reusable Graphql fragments 
    |-- /i18n
        |-- /locales
            |-- /en-us
            |-- /ko-kr
            |-- /zh-cn
            |-- /index.js # Modify and add new languages here, then add the corresponding language folder (e.g. /vi) with a structure mirroring /pages
    |-- /pages  # Everything under this folder becomes pages automatically
    |-- /templates  # Contains templates for programmatically creating pages (i.e. blog post with data fetched from CMS)
|-- /static  # Anything in here will not be processed by Webpack; will be copied into the public folder untouched
    |-- /_redirects  # Configure all redirects and rewrite rules here (list each redirect rule on a seperate line, with the original path followed by the new path or URL)
|-- .env.development
|-- .env.production
|-- gatsby-config.js  # Main configuration file for the site (plugins and site metadata)
|-- gatsby-node.js  # Customize the build process here (ie. config url language path and programmatically create blog posts from prismic source)
```
## Examples:
1. Create pages by sourcing content from Prismic
2. Create pages with JSON copywrite

### Example 1: Add blog post through Prismic
1.  **Create content in Prismic**
    Define custom type for blog post.
    - UID (unique ID): also used as the slug. different one for different translations
    - Title
    - Description
    - Published Date
    - Author
    - Hero (banner iamge)
    - Content
    
    Drag and drop 
    [![N|Solid](https://www.cybermiles.io/readme/prismic-ui.png)](https://www.cybermiles.io/readme/prismic-ui.png)
    Or use the JSON editor:
    ```javascript
    {
      "Main" : {
        "uid" : {
          "type" : "UID",
          "config" : {
            "label" : "ID",
            "placeholder" : "Unique ID (aka slug)"
          }
        },
        "title" : {
          "type" : "StructuredText",
          "config" : {
            "single" : "heading1",
            "label" : "Title",
            "placeholder" : "Title of post"
          }
        },
        "description" : {
          "type" : "StructuredText",
          "config" : {
            "multi" : "paragraph, preformatted, heading1, heading2, heading3, heading4, heading5, heading6, strong, em, hyperlink, image, embed, list-item, o-list-item, o-list-item",
            "allowTargetBlank" : true,
            "label" : "Description",
            "placeholder" : "Description of Post"
          }
        },
        "published_date" : {
          "type" : "Date",
          "config" : {
            "label" : "Published Date",
            "placeholder" : "Published Date"
          }
        },
        "author" : {
          "type" : "StructuredText",
          "config" : {
            "single" : "heading3",
            "label" : "Author",
            "placeholder" : "Author"
          }
        },
        "hero" : {
          "type" : "Image",
          "config" : {
            "constraint" : { },
            "thumbnails" : [ ],
            "label" : "Hero"
          }
        },
        "content" : {
          "type" : "StructuredText",
          "config" : {
            "multi" : "paragraph, preformatted, heading1, heading2, heading3, heading4, heading5, heading6, strong, em, hyperlink, image, embed, list-item, o-list-item, o-list-item",
            "allowTargetBlank" : true,
            "label" : "Content",
            "placeholder" : "Content"
          }
        }
      }
    }
    ```
    
    Head over to the content tab and create a new post using the 'Blog Post' type.
    Then proceed to create each post with its various translations.
2. **Create the blog post template**
   Open GraphiQL (in-browser IDE) at http://localhost:8000/___graphql to test the Prismic data and schema.
   ```javascript
   {
      allPrismicBlogPost(sort: {fields: [data___published_date], order: DESC}) {
        edges {
          node {
            uid
            lang
            data {
              title {
                text
              }
              published_date
            }
          }
        }
      }
    }
   ```
   The resulting data should look like the following
   ```javascript
    {
      "data": {
        "allPrismicBlogPost": {
          "edges": [
            {
              "node": {
                "uid": "going-the-extra-mile-january2019",
                "lang": "en-us",
                "data": {
                  "title": {
                    "text": "Going the Extra Mile (January 2019)"
                  },
                  "published_date": "2019-01-02"
                }
              }
            },
            {
              "node": {
                "uid": "关于cmt社区问题的回复",
                "lang": "zh-cn",
                "data": {
                  "title": {
                    "text": "关于CMT社区问题的回复"
                  },
                  "published_date": "2018-12-31"
                }
              }
            },
        ...
   ```
   Edit the above query until you retrieve all the content you need from Prismic. Then create a template for blog post.
   ```sh
   $ cd src/templates && touch post.jsx
   ```
   Paste the query onto the template and pass the data prop into the component.
   ```javascript
   export const pageQuery = graphql`
      query PostBySlug($uid: String!, $locale: String!) {
        site {
          siteMetadata {
            url
            twitter
          }
        }
        prismicBlogPost(uid: { eq: $uid }, lang: { eq: $locale }) {
          uid
          lang
          alternate_languages {
            uid
            lang
            type
          }
          data {
            title {
              text
            }
            description {
              text
            }
            author {
              text
            }
            hero {
              localFile {
                childImageSharp {
                  fluid(maxWidth: 1240, maxHeight: 356) {
                    ...GatsbyImageSharpFluid_tracedSVG
                  }
                }
              }
            }
            content {
              html
            }
          }
        }
      }
    `
   ```
   Destructure the content from 'data' and access it like you would for any regular variables
   ```javascript
   const { data } = this.props
   const { prismicBlogPost: post, site } = data
   ```
   ```javascript
    <h1>{post.data.title.text}</h1>
   ```
3.  **Create Blog Posts programmatically with the above Prismic data**
    Navigate to gatsby-node.js and create a new blog post page for each node
    ```javascript
    //gatsby-node.js
    exports.createPages = async ({ graphql, actions }) => {
      const { createPage } = actions
    
      const pages = await graphql(`
        {
          allPrismicBlogPost {
            edges {
              node {
                id
                uid
                lang
              }
            }
          }
        }
      `)
    
      const blogPostTemplate = path.resolve('src/templates/post.jsx')
    
      pages.data.allPrismicBlogPost.edges.forEach(edge => {
        console.log('Blog Page--: ', `/${edge.node.lang}/blog/${edge.node.uid}/`)
        createPage({
          path: `/${edge.node.lang}/blog/${edge.node.uid}/`,
          component: slash(blogPostTemplate),
          context: {
            locale: edge.node.lang,
            uid: edge.node.uid,
          },
        })
      })
    }
    ```
### Example: Add new page (e.g. MetaMask)
1.  **Add translations**
    
    Create the translation file.
    ```sh
    $ cd src/i18n/locales/en-us/blockchain-infrastructure
    $ touch metamask.js
    ```
    The following is a very inefficient and messy way of adding content to our site. Only strings are accepted and arrays are not recognized. We hope to migrate all copywrite over to Prismic (our CMS) in the near future so that we would not have to rely on react-intl to handle the translations.
    ```javascript
    // src/i18n/locales/en-us/blockchain-infrastructure/metamask.js
    module.exports = {
      title: 'MetaMask for CMT',  // Will be shown on browser tab
      'meta-description': 'Bring CyberMiles to your browser',  // used for SEO
      banner: {
        title: 'MetaMask for CMT',
        backgroundImage: 'bannerCMT',  // Corresponds to the src/assets/images/hero/bannerCMT@3x.png image used for the banner
        description: 'null',  // Unfortunately everything in this file must be in String (includes booleans, null, numbers, etc)
        buttons: {
          number: '0',  
        },
      },
      centered: {  // Corresponds to src/components/reusables/centered.jsx
        number: '1',  // react-intl could not handle arrays, so we need to specify the number of 'centered' sections for this page
        centered0: {  // Since we can't use arrays, the only option is to do this. Not ideal :(
          images: {
            number: '1',  
            image0: {
              gif: 'null',
              path: 'metamask',
              link: 'null',
            },
          },
          title: 'Brings CyberMiles to your browser',
          description: {
            richText: 'true',  // Instead of true vs false, we're using true vs null (both in strings). Will need to change this in the future
            description:
              `<p>MetaMask for CMT” is a bridge that allows you to visit the distributed web of tomorrow in your browser today. It allows you to run CyberMiles dApps right in your browser without running a CyberMiles node.</p><p>MetaMask includes a secure identity vault, providing a user interface to manage your identities on different sites and sign blockchain transactions.</p><p>You can install the MetaMask add-on in Chrome browser. If you’re a developer, you can start developing with MetaMask today.</p>`,
          },
          backgroundImage: 'null',
          buttons: {
            number: '1',
            button0: {
              link:
                'https://chrome.google.com/webstore/detail/metamask-for-cmt/hmiddckbbijmdkamphkgkelnjjdkicck',
              label: 'Get Chrome Extension',
              icon: 'null',
            },
          },
        },
      },
    }
    ```
    Copy and paste these files into the other locale folders and translate the content.
    ```sh
    # From the root
    $ sudo cp src/i18n/locales/en-us/blockchain-infrastructure/metamask.js src/i18n/locales/zh-cn/blockchain-infrastructure
    ```
    
    The metamask page will have access to the following context methods and variables after the react-intl injection.
    [![N|Solid](https://www.cybermiles.io/readme/react-intl-log.png)](https://www.cybermiles.io/readme/react-intl-log.png)
2. **Add the Page**

    Gatsby automatically generates everything under the src/pages directory into pages. (using react-router behind the scenes)
    ```sh
    $ cd src/pages/blockchain-infrastructure
    $ touch metamask.jsx
    ```
    Query the required images with graphql and specify the dimensions and loading effect (blur or svg outline). We're using fragments for bannerCMT.png and metamask.png in this case since they are reused in other pages.
    ```javascript
    // src/pages/metamask.jsx
    export const imagesQuery = graphql`
      query {
        bannerCMT: file(relativePath: { eq: "hero/bannerCMT@3x.png" }) {
          ...bannerIcon
        }
        metamask: file(relativePath: { eq: "regular/metamask.png" }) {
          ...fullWidth
        }
        shortLastSection: file(relativePath: { eq: "hero/shortLastSection@2x.png" }) {
          childImageSharp {
            fluid(maxHeight: 400) {
              ...GatsbyImageSharpFluid
            }
          }
        }
      }
    `
    ```
    Pass the data as props to the react components. data contains the queried images.
    ```javascript
    // src/pages/metamask.jsx
    const CmtWallet = ({ data }) => (
      <Layout>
        <Banner data={data} shade={'light'} />
        <Centered index={0} data={data} shade={'light'} />
      </Layout>
    )
    ```
    Inject react-intl into the component and format the content with the formatMessage API method.
    ```javascript
    // src/components/reusables/banner.jsx
    const Banner = ({ data, shade, intl }) => {
      const title = intl.formatMessage({
        id: `banner.title`,
      })
      const hasTitle = title !== 'null'
      const description = intl.formatMessage({
        id: `banner.description`,
      })
      const hasDescription = description !== 'null'
      const buttons = intl.formatMessage({
        id: `banner.buttons.number`,
      })
      const backgroundImage = intl.formatMessage({
        id: `banner.backgroundImage`,
      })
      const backgroundPath = data[`${backgroundImage}`].childImageSharp.fixed
      
      return (...)
    }
    
    Banner.propTypes = {
      data: PropTypes.object,
      shade: PropTypes.string,
      intl: intlShape.isRequired,
    }
    
    export default injectIntl(Banner)
    ```
    Everything else is regular [ReactJS] + [Styled Components]
3. **Add page to Navigation**

    ```sh
    $ cd src/i18n/locales/en-us/navigation/header.js
    ```
    Add the following to the corresponding location
    ```javascript
    {
      disabled: 'false',
      label: 'MetaMask',
      link: 'blockchain-infrastructure/metamask/',
      children: [],
    }
    ```
### Resources:
API Keys and Accounts information: 
https://5milesrd.quip.com/OcDAOAbC9R0
### Todos

 - Refactor styles and code that are used over and over again
 - Add breadcrumbs to blog posts
 - Migrate all copywrite and content over to prismic

[//]: #
   
   [GatsbyJS]: <https://www.gatsbyjs.org/>
   [ReactJS]: <https://reactjs.org/>
   [GraphQL]: <https://graphql.org/>
   [react-intl]: <https://github.com/yahoo/react-intl>
   [Styled Components]: <https://www.styled-components.com/>
   [Grommet]: <https://v2.grommet.io/>
   [Netlify]: <https://www.netlify.com/>
   [Prismic]: <https://prismic.io/>
   [Zapier]: <https://zapier.com/>
