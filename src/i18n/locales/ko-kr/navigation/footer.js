module.exports = {
  title: 'Footer',
  'meta-description': 'Footer',
  address:
    "CyberMiles Foundation Limited - 13/F Gloucester Tower, The Landmark, 15 Queen's Road Central, Hong Kong",
  copyright: 'Copyright © 2018. All rights reserved',
  subscribe: {
    label: '구독하기',
    title: 'CyberMiles최신 뉴스'
  },
  links: {
    contact: {
      label: '연락처',
      link: '/about-us/contact-us',
    },
    privacy: {
      label: '개인정보',
      link: '/privacy',
    },
    terms: {
      label: '조항',
      link: '/terms',
    },
  },
  iconsTitle: "공식 커뮤니티 플랫폼"
}
