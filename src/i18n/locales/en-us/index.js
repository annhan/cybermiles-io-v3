module.exports = {
  title: 'Cybermiles',
  'meta-description':
    'CyberMiles is a smart, fast, secure, and free blockchain platform built for e-commerce with lightning-fast transaction speed and smart contract templates.',
  banner: {
    title: 'CyberMiles',
    backgroundImage: 'bannerCube',
    description: 'Public Blockchain for E-commerce',
    buttons: {
      number: '1',
      button0: {
        label: 'How CyberMiles Works',
        link: '/',
        icon: 'play',
      },
    },
  },
  imageText: {
    section0: {
      image: {
        gif: 'true',
        path: 'cube',
      },
      title: 'Best Blockchain Solutions for E-commerce',
      description: {
        richtext: 'null',
        description:
          'CyberMiles has built highly customized solutions for the e-commerce companies to deploy their business on the Blockchain with ease.',
      },
      backgroundImage: 'null',
      buttons: {
        number: '1',
        button0: {
          label: 'Find your use cases',
          link: 'ecommerce-solutions/finance/payment-gateway/',
        },
      },
    },
    section1: {
      image: {
        gif: 'true',
        path: 'coin',
      },
      title: 'Valuable Blockchain Technology',
      description: {
        richtext: 'null',
        description:
          'CyberMiles is a Smart, Fast, and Safe blockchain network while staying Free to everyone.',
      },
      backgroundImage: 'null',
      buttons: {
        number: '1',
        button0: {
          label: 'Buy and Sell CMT',
          link: 'cmt/overview/',
        },
      },
    },
    section2: {
      image: {
        gif: 'true',
        path: 'lity',
      },
      title: 'Simple Development on Blockchain',
      description: {
        richtext: 'null',
        description:
          'CyberMiles has built one of the most powerful infrastructure for developing Decentralised applications.',
      },
      backgroundImage: 'null',
      buttons: {
        number: '1',
        button0: {
          label: 'Build with CyberMiles',
          link: 'developer-portal/developer-hub',
        },
      },
    },
  },
  roadmap: {
    title: 'RoadMap',
    description: {
      richtext: 'null',
      description: 'null',
    },
    backgroundImage: 'lastSection',
    buttons: {
      number: '1',
      button0: {
        label: 'View More',
        link: '/about-us/roadmap',
      },
    },
    events: {
      number: '4',
      event0: {
        status: 'Future',
        date: '2019 Jan',
        description:
          'Completes a minimal viable ecosystem (MVE) for on-chain tokens and exchanges',
      },
      event1: {
        status: 'Future',
        date: '2019 Feb',
        description:
          'Incorporates over 15M American consumers’ data onto the CyberMiles public blockchain',
      },
      event2: {
        status: 'Future',
        date: '2019 April',
        description: 'Supports over 100 DApps',
      },
      event3: {
        status: 'Future',
        date: '2019 June',
        description:
          'Onboards first 50 business partners for e-commerce applications',
      },
    },
  },
  centered: {
    centered0: {
      image: 'null',
      title: 'Start Working With CyberMiles',
      description: {
        richText: 'null',
        description:
          'Any interesting ideas? Learn more or speak with our team member.',
      },
      backgroundImage: 'null',
      buttons: {
        number: '1',
        button0: {
          label: 'Contact Us',
          link: '/about-us/contact-us',
        },
      },
    },
  },
}
