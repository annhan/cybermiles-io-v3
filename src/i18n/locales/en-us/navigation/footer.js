module.exports = {
  title: 'Footer',
  'meta-description': 'Footer',
  address:
    "CyberMiles Foundation Limited - 13/F Gloucester Tower, The Landmark, 15 Queen's Road Central, Hong Kong",
  copyright: 'Copyright © 2018. All rights reserved',
  subscribe: {
    label: 'Subscribe',
    title: 'Get the latest CyberMiles news'
  },
  links: {
    contact: {
      label: 'Contact',
      link: '/about-us/contact-us',
    },
    privacy: {
      label: 'Privacy',
      link: '/privacy',
    },
    terms: {
      label: 'Terms',
      link: '/terms',
    },
  },
  iconsTitle: "Follow Us"
}
