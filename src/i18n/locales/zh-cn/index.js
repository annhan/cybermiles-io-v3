module.exports = {
  title: 'Cybermiles',
  'meta-description': '专为电商的区块链',
  banner: {
    title: 'CyberMiles',
    backgroundImage: 'bannerCube',
    description: '专为电商的公链',
    buttons: {
      number: '1',
      button0: {
        label: 'CyberMiles公链是什么',
        link: '/',
        icon: 'play',
      },
    },
  },
  imageText: {
    section0: {
      image: {
        gif: 'true',
        path: 'cube',
      },
      title: '电商的最佳区块链解决方案',
      description: {
        richtext: 'null',
        description:
          'CyberMiles公链为电商企业推出了高度定制的解决方案，以便电商企业能够轻松将业务部署在区块链上。',
      },
      backgroundImage: 'null',
      buttons: {
        number: '1',
        button0: {
          label: '找到你的用例',
          link: 'ecommerce-solutions/finance/payment-gateway/',
        },
      },
    },
    section1: {
      image: {
        gif: 'true',
        path: 'coin',
      },
      title: '有价值的区块链技术',
      description: {
        richtext: 'null',
        description:
          'CyberMiles公链是智能、快速、安全的区块链网络，并且对所有人免费。',
      },
      backgroundImage: 'null',
      buttons: {
        number: '1',
        button0: {
          label: '购买CMT',
          link: 'cmt/overview/',
        },
      },
    },
    section2: {
      image: {
        gif: 'true',
        path: 'lity',
      },
      title: '区块链上的简单开发',
      description: {
        richtext: 'null',
        description: 'CyberMiles公链构建了强有力的开发Dapp的基础架构。',
      },
      backgroundImage: 'null',
      buttons: {
        number: '1',
        button0: {
          label: '用CyberMiles开发',
          link: 'developer-portal/developer-hub',
        },
      },
    },
  },
  roadmap: {
    title: '路线图',
    description: {
      richtext: 'null',
      description: 'null',
    },
    backgroundImage: 'lastSection',
    buttons: {
      number: '1',
      button0: {
        label: '查看更多',
        link: '/about-us/roadmap',
      },
    },
    events: {
      number: '4',
      event0: {
        status: 'Future',
        date: '2019 Jan',
        description: '为链上代币和交易完成最小可行生态系统',
      },
      event1: {
        status: 'Future',
        date: '2019 Feb',
        description: '整合超过1500万美国消费者的数据到CyberMiles 区块链',
      },
      event2: {
        status: 'Future',
        date: '2019 April',
        description: '支持超过100个去中心化应用（DApps）',
      },
      event3: {
        status: 'Future',
        date: '2019 June',
        description: '达到首批50个电商应用的商业合作伙伴',
      },
    },
  },
  centered: {
    centered0: {
      image: 'null',
      title: '与CyberMiles公链一起合作',
      description: {
        richText: 'null',
        description: '任何有趣的想法？来和我们分享！',
      },
      backgroundImage: 'null',
      buttons: {
        number: '1',
        button0: {
          label: '联系我们',
          link: '/about-us/contact-us',
        },
      },
    },
  },
}
