module.exports = {
  title: '路线图',
  'meta-description': '路线图',
  banner: {
    title: '路线图',
    backgroundImage: 'bannerGlobe',
    description: 'null',
    buttons: {
      number: '0',
    },
  },
  roadmap: {
    title: 'null',
    description: {
      richtext: 'null',
      description: 'null',
    },
    backgroundImage: 'lastSection',
    buttons: {
      number: '0',
    },
    events: {
      number: '12',
      // event0: {
      //   status: 'Past',
      //   date: '2018 Dec',
      //   description:
      //     'Supports multiple real-world e-commerce scenarios including CMT payments on blocktonic, Lightinthebox, and Dallas Mavericks tickets',
      // },
      event0: {
        status: 'Future',
        date: '2019 一月',
        description: '为链上代币和交易完成最小可行生态系统',
      },
      event1: {
        status: 'Future',
        date: '2019 二月',
        description: '整合超过1500万美国消费者的数据到CyberMiles公链',
      },
      event2: {
        status: 'Future',
        date: '2019 四月',
        description: '支持超过100个去中心化应用（DApps）',
      },
      event3: {
        status: 'Future',
        date: '2019 六月',
        description: '达到首批50个电商应用的商业合作伙伴',
      },
      event4: {
        status: 'Future',
        date: '2019 七月',
        description:
          '每日交易量维持在以太坊交易量的两倍以上',
      },
      event5: {
        status: 'Future',
        date: '2019 八月',
        description: '开发者社区达到2,000人',
      },
      event6: {
        status: 'Future',
        date: '2019 九月',
        description: '日活跃用户超过以太坊和EOS之和',
      },
      event7: {
        status: 'Future',
        date: '2019 十月',
        description: '所有的电商交易支持稳定币',
      },
      event8: {
        status: 'Future',
        date: '2019 Q4',
        description:
          '成为全球顶级证券代币发行方之一',
      },
      event9: {
        status: 'Future',
        date: '2020 Q1',
        description:
          '来自开发者社区的电商虚拟机优化超过100项',
      },
      event10: {
        status: 'Future',
        date: '2020 Q2',
        description:
          '启动全球企业级服务，推广公链服务',
      },
      event11: {
        status: 'Future',
        date: '2020 Q3',
        description:
          '成为远东、东南亚、南亚和非洲的顶级电子商务交易清算中心',
      },
      event12: {
        status: 'Future',
        date: '2020 Q4',
        description: '去中心化电商被广泛采用',
      },
    },
  },
}
