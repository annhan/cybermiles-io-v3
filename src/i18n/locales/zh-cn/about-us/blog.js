module.exports = {
  title: '博客',
  'meta-description': 'Stay tuned and read the latest Cybermiles feature updates, official announcements, blockchain news.',
  banner: {
    title: '博客',
    backgroundImage: 'bannerGlobe',
    description:
      'null',
    buttons: {
      number: '0',
    }
  },
}
