import { graphql } from 'gatsby'

export const fluidImage = graphql`
  fragment fluidImage on File {
    childImageSharp {
      fluid(maxWidth: 1000) {
        ...GatsbyImageSharpFluid_tracedSVG
      }
    }
  }
`

export const fourColumns = graphql`
  fragment fourColumns on File {
    childImageSharp {
      fixed(width: 190) {
        ...GatsbyImageSharpFixed_tracedSVG
      }
    }
  }
`

export const logos = graphql`
  fragment logos on File {
    childImageSharp {
      fixed(width: 200) {
        ...GatsbyImageSharpFixed_tracedSVG
      }
    }
  }
`
export const bannerIcon = graphql`
  fragment bannerIcon on File {
    childImageSharp {
      fixed(width: 180) {
        ...GatsbyImageSharpFixed_tracedSVG
      }
    }
  }
`

export const footer = graphql`
  fragment footer on File {
    childImageSharp {
      fluid(maxWidth: 44) {
        ...GatsbyImageSharpFluid_tracedSVG
      }
    }
  }
`

export const people = graphql`
  fragment people on File {
    childImageSharp {
      fluid(maxWidth: 180, maxHeight: 180) {
        ...GatsbyImageSharpFluid_tracedSVG
      }
    }
  }
`

export const solution = graphql`
  fragment solution on File {
    childImageSharp {
      fixed(width: 400, height: 200) {
        ...GatsbyImageSharpFixed_tracedSVG
      }
    }
  }
`

export const caseStudies = graphql`
  fragment caseStudies on File {
    childImageSharp {
      fixed(height: 40) {
        ...GatsbyImageSharpFixed_tracedSVG
      }
    }
  }
`

export const buttonIcon = graphql`
  fragment buttonIcon on File {
    childImageSharp {
      fixed(height: 18) {
        ...GatsbyImageSharpFixed_tracedSVG
      }
    }
  }
`

export const step = graphql`
  fragment step on File {
    childImageSharp {
      fixed(height: 72) {
        ...GatsbyImageSharpFixed_tracedSVG
      }
    }
  }
`

export const pdf = graphql`
  fragment pdf on File {
    childImageSharp {
      fixed(width: 273, height: 387) {
        ...GatsbyImageSharpFixed_tracedSVG
      }
    }
  }
`

export const phone = graphql`
  fragment phone on File {
    childImageSharp {
      fixed(height: 351) {
        ...GatsbyImageSharpFixed_tracedSVG
      }
    }
  }
`

export const feature = graphql`
  fragment feature on File {
    childImageSharp {
      fixed(height: 72) {
        ...GatsbyImageSharpFixed_tracedSVG
      }
    }
  }
`

export const downloadButton = graphql`
  fragment downloadButton on File {
    childImageSharp {
      fixed(height: 44) {
        ...GatsbyImageSharpFixed_tracedSVG
      }
    }
  }
`

export const fullWidth = graphql`
  fragment fullWidth on File {
    childImageSharp {
      fluid(maxWidth: 1000) {
        ...GatsbyImageSharpFluid_tracedSVG
      }
    }
  }
`
