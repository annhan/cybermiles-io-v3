import 'antd/lib/timeline/style/index.css'

import { FormattedMessage, injectIntl, intlShape } from 'react-intl'
import React, { Component } from 'react'
import {
  StyledContainer,
  StyledH3,
  StyledP,
  StyledRowButtonContainer,
  StyledSpan,
  StyledUnderlinedH2
} from './styles'

import { Box } from 'grommet'
import Button from '../reusables/button'
import PropTypes from 'prop-types'
import Timeline from 'antd/lib/timeline'
import media from 'styled-media-query'
import shortid from 'shortid'
import styled from 'styled-components'

const StyledEvent = styled(Box)`
  ${media.greaterThan('medium')`
    border: 1px solid ${props => props.theme.global.colors.weakGrey};
    padding: 24px;
  `}
`

const StyledDate = styled(StyledH3)`
  color: ${props => props.theme.global.colors.primaryBlack};
  text-transform: none;
  font-weight: 500;
`

const StyledInnerContainer = styled(Timeline)`
  font-family: 'Montserrat', -apple-system, BlinkMacSystemFont, 'Segoe UI',
    Roboto, Oxygen, Ubuntu, Cantarell, 'Fira Sans', 'Droid Sans',
    'Helvetica Neue', Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji',
    'Segoe UI Symbol' !important;

  .ant-timeline-item-tail {
    height: 80%;
    bottom: 5% !important;
    top: auto !important;
  }

  ${media.greaterThan('medium')`
    .ant-timeline-item-left {
      .ant-timeline-item-content {
        margin-left: 80px !important;
        max-width: 40% !important;
      }
    }
  
    .ant-timeline-item-right {
      .ant-timeline-item-content {
        left: 0 !important;
        max-width: 40% !important;
        text-align: left !important;
      }
    }
  
    .ant-timeline-item-head {
      margin-left: -7px !important;
      width: 16px;
      height: 16px;
    }
  `}
`

const StyledTimeline = ({ data, shade, intl }) => {
  const title = intl.formatMessage({
    id: `roadmap.title`,
  })
  const hasTitle = title !== 'null'
  const description = intl.formatMessage({
    id: `roadmap.description.description`,
  })
  const hasDescription = description !== 'null'
  const isRichText =
    intl.formatMessage({
      id: `roadmap.description.richText`,
    }) !== 'null'
  const events = intl.formatMessage({
    id: `roadmap.events.number`,
  })
  const buttons = intl.formatMessage({
    id: `roadmap.buttons.number`,
  })
  const backgroundImage = intl.formatMessage({
    id: `roadmap.backgroundImage`,
  })
  return (
    <StyledContainer
      componentName="Timeline"
      shade={shade}
      data={data}
      backgroundImage={backgroundImage}
    >
      {hasTitle && (
        <StyledUnderlinedH2 shade={shade}>{title}</StyledUnderlinedH2>
      )}
      {hasDescription && !isRichText ? (
        <StyledSpan>{description}</StyledSpan>
      ) : (
        hasDescription &&
        isRichText && (
          <StyledSpan
            dangerouslySetInnerHTML={{ __html: description }}
          />
        )
      )}
      <StyledInnerContainer mode="alternate">
        {Array.from(Array(Number(events)).keys()).map((event, index) => {
          const status = intl.formatMessage({
            id: `roadmap.events.event${index}.status`,
          })
          const date = intl.formatMessage({
            id: `roadmap.events.event${index}.date`,
          })
          const description = intl.formatMessage({
            id: `roadmap.events.event${index}.description`,
          })
          return (
            <Timeline.Item
              key={shortid.generate()}
              color={status === 'Past' ? 'green' : 'blue'}
            >
              <StyledEvent>
                <StyledDate style={{ margin: '0' }}>{date}</StyledDate>
                <StyledP style={{ margin: '10px 0 0 0' }}>
                  {description}
                </StyledP>
              </StyledEvent>
            </Timeline.Item>
          )
        })}
      </StyledInnerContainer>
      {buttons > 0 && (
        <StyledRowButtonContainer buttons={buttons}>
          {Array.from(Array(Number(buttons)).keys()).map(i => {
            const button = `button${i}`
            const buttonLabel = intl.formatMessage({
              id: `roadmap.buttons.${button}.label`,
            })
            const buttonLink = intl.formatMessage({
              id: `roadmap.buttons.${button}.link`,
            })
            return (
              <Button
                key={shortid.generate()}
                label={buttonLabel}
                link={buttonLink}
                light={shade === 'light' || shade === 'grey' ? true : false}
              />
            )
          })}
        </StyledRowButtonContainer>
      )}
    </StyledContainer>
  )
}

StyledTimeline.propTypes = {
  shade: PropTypes.string,
  data: PropTypes.object,
  intl: intlShape.isRequired,
}

export default injectIntl(StyledTimeline)
