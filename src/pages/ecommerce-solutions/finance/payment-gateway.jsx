import { bannerIcon, caseStudies, fourColumns, logos, solution } from '../../../fragments/fragments'

import Addition from '../../../components/technical-benchmark/addition'
import Banner from '../../../components/reusables/banner'
import Centered from '../../../components/reusables/centered'
import Columns from '../../../components/reusables/columns'
import IconColumns from '../../../components/reusables/iconColumns'
import Layout from '../../../components/layout'
import Multiplcation from '../../../components/technical-benchmark/multiplication'
import React from 'react'
import Solutions from '../../../components/reusables/solutions'
import Tabs from '../../../components/reusables/tabs'
import { graphql } from 'gatsby'
import { injectIntl } from 'react-intl'
import { withIntl } from '../../../i18n'

const PaymentGateway = ({ data }) => {
  const WrappedTabs = Tabs([<Addition />, <Multiplcation />])
  return (
    <Layout>
      <Banner data={data} shade={'grey'} />
      <Centered index="0" data={data} shade={'light'} />
      <Solutions index="0" data={data} shade={'grey'} />
      <Columns index="0" data={data} shade={'light'} />
      {/* <WrappedTabs /> */}
    </Layout>
  )
}

export default withIntl(PaymentGateway)

export const imagesQuery = graphql`
  query {
    bannerCube: file(relativePath: { eq: "hero/bannerCube@3x.png" }) {
      ...bannerIcon
    }
    lightInTheBox: file(relativePath: { eq: "lightInTheBox@3x.png" }) {
      ...caseStudies
    }
    blocktonic: file(relativePath: { eq: "blocktonic@3x.png" }) {
      ...caseStudies
    }
    gateway_love: file(relativePath: { eq: "regular/gateway-love@3x.png" }) {
      ...solution
    }
    gateway_coin: file(relativePath: { eq: "regular/gateway-coin@3x.png" }) {
      ...solution
    }
    gateway_refresh: file(relativePath: { eq: "regular/gateway-refresh@3x.png" }) {
      ...solution
    }
    shortLastSection: file(relativePath: { eq: "hero/shortLastSection@2x.png" }) {
      childImageSharp {
        fluid(maxHeight: 400) {
          ...GatsbyImageSharpFluid
        }
      }
    }
  }
`