import {
  bannerIcon,
  fourColumns,
  logos,
  solution,
} from '../../../fragments/fragments'

import Addition from '../../../components/technical-benchmark/addition'
import Banner from '../../../components/reusables/banner'
import Centered from '../../../components/reusables/centered'
import Layout from '../../../components/layout'
import Multiplcation from '../../../components/technical-benchmark/multiplication'
import React from 'react'
import Solutions from '../../../components/reusables/solutions'
import Tabs from '../../../components/reusables/tabs'
import { graphql } from 'gatsby'
import { injectIntl } from 'react-intl'
import { withIntl } from '../../../i18n'

const StableCoin = ({ data }) => {
  const WrappedTabs = Tabs([<Addition />, <Multiplcation />])
  return (
    <Layout>
      <Banner data={data} shade={'grey'} />
      <Centered index="0" data={data} shade={'light'} />
      <Solutions index="0" data={data} shade={'grey'} />
    </Layout>
  )
}

export default withIntl(StableCoin)

export const imagesQuery = graphql`
  query {
    bannerCube: file(relativePath: { eq: "hero/bannerCube@3x.png" }) {
      ...bannerIcon
    }
    fivemiles: file(relativePath: { eq: "partners/fivemiles@3x.png" }) {
      ...logos
    }
    gateway_symbol: file(
      relativePath: { eq: "regular/gateway-symbol@3x.png" }
    ) {
      ...solution
    }
    gateway_link: file(relativePath: { eq: "regular/gateway-link@3x.png" }) {
      ...solution
    }
    gateway_otc: file(relativePath: { eq: "regular/gateway-otc@3x.png" }) {
      ...solution
    }
    shortLastSection: file(relativePath: { eq: "hero/shortLastSection@2x.png" }) {
      childImageSharp {
        fluid(maxHeight: 400) {
          ...GatsbyImageSharpFluid
        }
      }
    }
  }
`
