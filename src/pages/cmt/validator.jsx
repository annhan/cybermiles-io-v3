import { Link, withIntl } from '../../i18n'
import { StaticQuery, graphql } from 'gatsby'
import {
  bannerIcon,
  downloadButton,
  feature,
  fluidImage,
  phone,
} from '../../fragments/fragments'

import Accordion from '../../components/reusables/accordion'
import Banner from '../../components/reusables/banner'
import Centered from '../../components/reusables/centered'
import Features from '../../components/reusables/features'
import { FormattedMessage } from 'react-intl'
import Hero from '../../components/reusables/hero'
import ImageText from '../../components/reusables/imageText'
import ImageTextHero from '../../components/reusables/imageTextHero'
import IndexMenu from '../../components/reusables/indexMenu'
import Layout from '../../components/layout'
import React from 'react'
import Rows from '../../components/reusables/rows'

const Validator = ({ data }) => (
  <Layout>
    <Banner data={data} shade={'light'} />
    <Centered index={0} data={data} shade={'light'} />
    <Centered index={1} data={data} shade={'grey'} />
    <Centered index={2} data={data} shade={'light'} />
    <Accordion index={0} data={data} shade={'grey'} />
  </Layout>
)

export default withIntl(Validator)

export const imagesQuery = graphql`
  query {
    bannerCoin: file(relativePath: { eq: "hero/bannerCoin@3x.png" }) {
      ...bannerIcon
    }    
    shortLastSection: file(relativePath: { eq: "hero/shortLastSection@2x.png" }) {
      childImageSharp {
        fluid(maxHeight: 400) {
          ...GatsbyImageSharpFluid
        }
      }
    }
  }
`
