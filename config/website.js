module.exports = {
  _pathPrefix: '/', // Prefix for all links. If you deploy your site to example.com/portfolio your pathPrefix should be "portfolio"
  _title: 'Cybermiles', // Navigation and Site Title
  _titleAlt: 'Cybermiles', // Title for JSONLD
  description: 'Blockchain for E-commerce',
  _url: 'https://www.cybermiles.io', // Domain of your site. No trailing slash!
  siteLanguage: 'en', // Language Tag on <html> element
  logo: '/twitterCardLogo.jpg', // Used for SEO

  // JSONLD / Manifest
  favicon: 'src/assets/images/mobileLogo.png', // Used for manifest favicon generation
  shortName: 'CyberMiles', // shortname for manifest. MUST be shorter than 12 characters
  author: 'CyberMiles', // Author for schemaORGJSONLD
  themeColor: '#3D63AE',
  backgroundColor: '#EBEDF2',

  twitter: '@cybermiles', // Twitter Username
};