require('dotenv').config({
  path: `.env.${process.env.NODE_ENV}`,
})

const path = require('path')
const _ = require(`lodash`)
const Promise = require(`bluebird`)
const slash = require(`slash`)
const { languages } = require('./src/i18n/locales')
const request = require('request-promise')
const crypto = require('crypto')

exports.sourceNodes = async ({ boundActionCreators }) => {
  const { createNode } = boundActionCreators

  const requestOptions = {
    method: 'GET',
    uri: `https://${process.env.COIN_MARKET_CAP_API}/v1/cryptocurrency/quotes/latest`,
    qs: {
      symbol: 'CMT',
      convert: 'USD',
    },
    headers: {
      'X-CMC_PRO_API_KEY': process.env.X_CMC_PROP_API_KEY,
    },
    json: true,
    gzip: true,
  }

  const res = await request(requestOptions)

  Object.entries(res.data).map(([key, value]) => {
    const { id, name, symbol, quote } = value
    const { price, volume_24h, market_cap } = quote.USD

    const coinNode = {
      id: symbol,
      parent: null,
      internal: {
        type: `coins`,
        contentDigest: crypto
          .createHash(`md5`)
          .update(JSON.stringify(value))
          .digest(`hex`),
      },
      children: [],
      name,
      symbol,
      price,
      volume: volume_24h,
      market_cap,
    }

    createNode(coinNode)
  })

  return
}

exports.createPages = async ({ graphql, actions }) => {
  const { createPage } = actions

  const pages = await graphql(`
    {
      allPrismicBlogPost {
        edges {
          node {
            id
            uid
            lang
          }
        }
      }
    }
  `)

  const blogPostTemplate = path.resolve('src/templates/post.jsx')

  pages.data.allPrismicBlogPost.edges.forEach(edge => {
    console.log('Blog Page--: ', `/${edge.node.lang}/blog/${edge.node.uid}/`)
    createPage({
      path: `/${edge.node.lang}/blog/${edge.node.uid}/`,
      component: slash(blogPostTemplate),
      context: {
        locale: edge.node.lang,
        uid: edge.node.uid,
      },
    })
  })
}

exports.onCreatePage = ({ page, boundActionCreators }) => {
  const { createPage, deletePage } = boundActionCreators

  if (page.path.includes('404')) {
    return Promise.resolve()
  }

  return new Promise(resolve => {
    const redirect = path.resolve('src/i18n/redirect.js')
    const redirectPage = {
      ...page,
      component: redirect,
      context: {
        languages,
        locale: '',
        routed: false,
        redirectPage: page.path,
      },
    }
    deletePage(page)
    createPage(redirectPage)

    languages.forEach(({ value }) => {
      console.log('Page--: ', `/${value}${page.path}`)
      const localePage = {
        ...page,
        originalPath: page.path,
        path: `/${value}${page.path}`,
        context: {
          languages,
          locale: value,
          routed: true,
          originalPath: page.path,
        },
      }
      createPage(localePage)
    })

    resolve()
  })
}
